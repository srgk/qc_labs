import pprint
import os
import json
from typing import Sequence, List

from qc_lab3.utils import (
    parse_tabbed_ints, bit_vector_to_int, int_to_bit_vector
)
from qc_lab3.logic import ElementCoverage
from qc_lab3.circuit_builder import build_circuit, parse_connections
from qc_lab3.faults import Fault


def parse_coverage_matrix(tabbed_input: str) -> Sequence[ElementCoverage]:
    raw_matrix = (row.split('\t') for row in tabbed_input.split('\n'))
    transposed_covs = zip(*raw_matrix)

    coverage = []  # type: List[ElementCoverage]

    for row in transposed_covs:
        el_coverage = list()  # type: List[int]

        for col in row:
            el_coverage.append(col if col == '•' else int(col))

        coverage.append(el_coverage)

    return coverage


def parse_faults(faults: str) -> Sequence[Fault]:
    faults_str = faults.split('\t')
    n_faults = len(faults_str)
    result = [None for _ in range(n_faults)]  # type: List[Fault]

    for line in range(n_faults):
        if faults_str[line] == '0':
            result[line] = False
        elif faults_str[line] == '1':
            result[line] = True

    return result


def parse_input_matrix(tabbed_input: str) -> Sequence[Sequence[int]]:
    rows = tabbed_input.split('\n')
    return [parse_tabbed_ints(row) for row in rows]


def simulate(
        connections: Sequence[str],
        coverage: Sequence[ElementCoverage],
        outputs: Sequence[int],
        input_values: Sequence[Sequence[int]],
        faults: Sequence[Fault] = None
) -> Sequence[Sequence[int]]:
    circuit = build_circuit(
        connections=connections,
        coverage=coverage,
        outputs=outputs,
        faults=faults
    )
    results = []

    for row in input_values:
        circuit.set_inputs(row)
        results.append([int(el) for el in circuit.get_line_values()])

    return results


def get_difference(
        matrix1: Sequence[Sequence[int]],
        matrix2: Sequence[Sequence[int]]
):
    result_matrix = []  # type: List[List[int]]

    for row1, row2 in zip(matrix1, matrix2):
        result_row = [a ^ b for a, b in zip(row1, row2)]
        result_matrix.append(result_row)

    return result_matrix


def get_activation_matrix(
        diffs: Sequence[Sequence[int]],
        reachability: Sequence[Sequence[int]]
) -> Sequence[Sequence[int]]:
    result = []  # type: List[Sequence[int]]

    if len(diffs) < 1 or len(reachability) < 1:
        return []

    if len(diffs[0]) != len(reachability):
        raise ValueError("Matrices are not compatible")

    n_tests = len(diffs)  # == number of rows in the matrix of diffs
    n_outs = len(reachability)  # == number of rows in the reachability matrix
    n_lines = len(reachability[0])  # == number of cols in the reachability mat

    for i_test in range(n_tests):
        pos_vector = 0  # type: int
        inv_vector = 0  # type: int

        for i_out in range(n_outs):
            el = bit_vector_to_int(reachability[i_out])

            if diffs[i_test][i_out]:  # != 0
                pos_vector = pos_vector | el

            else:
                inv_vector = inv_vector | el

        reduced = pos_vector & ~inv_vector
        result.append(int_to_bit_vector(reduced, n_lines))

    return result


def parse_fault_matrix(fault_table: str) -> Sequence[Sequence[str]]:
    result = []  # type: List[Sequence[str]]

    for row in fault_table.split('\n'):
        faults = row.split('\t')
        result.append(faults)

    return result


def get_coded_fault_matrix(
        fault_table: Sequence[Sequence[str]]
) -> Sequence[Sequence[int]]:
    mapping = {
        '•': 0b00,
        '1': 0b01,
        '0': 0b10,
        'X': 0b11
    }

    result = []  # type: List[List[int]]

    for src_row in fault_table:
        res_row = [mapping.get(el, 0b00) for el in src_row]
        result.append(res_row)

    return result


def hash_operation(s_element: int, f_element: int) -> int:
    lookup_table = [
        [0b11, 0b10, 0b01, 0b00],
        [0b11, 0b01, 0b10, 0b11]
    ]
    return lookup_table[s_element][f_element]


def hash_superposition(
        s_matrix: Sequence[Sequence[int]],
        f_matrix: Sequence[Sequence[int]]
) -> Sequence[Sequence[int]]:
    result = []  # type: List[Sequence[int]]

    for s_row, f_row in zip(s_matrix, f_matrix):
        hash_row = [
            hash_operation(s_el, f_el) for s_el, f_el in zip(s_row, f_row)
        ]
        result.append(hash_row)

    return result


def get_coded_fault_list(
        hashed_matrix: Sequence[Sequence[int]]
) -> Sequence[int]:
    n_rows = len(hashed_matrix)
    assert n_rows > 0
    n_cols = len(hashed_matrix[0])
    coded_faults = []  # type: List[int]

    for i_col in range(n_cols):
        reduced = 0b11

        for i_row in range(n_rows):
            reduced = reduced & hashed_matrix[i_row][i_col]

        coded_faults.append(reduced)

    return coded_faults


def decode_fault_list(coded_faults: Sequence[int]) -> Sequence[str]:
    mapping = {
        0b00: '•',
        0b01: '1',
        0b10: '0',
        0b11: 'X'
    }

    return [mapping.get(el, '•') for el in coded_faults]


def main():
    tabbed_cons = "•	•	•	•	•	•	3,4	10,7	7,5	1,2	8,6	9,6"
    tabbed_covs = """•	•	•	•	•	•	0	0	0	0	1	1
•	•	•	•	•	•	0	1	1	0	1	1
•	•	•	•	•	•	0	1	1	0	1	1
•	•	•	•	•	•	1	1	1	1	0	0"""
    tabbed_inputs = """1	1	1	1	0	1
1	1	0	0	1	1
0	1	0	1	0	1
1	0	1	0	1	0
0	0	0	0	1	1"""
    output_strings = "10,11,12"
    tabbed_faults = '•	•	•	•	•	•	•	1	•	0	•	•'

    ###########################################################################

    connections = tabbed_cons.split('\t')
    input_values = parse_input_matrix(tabbed_inputs)
    outputs = parse_connections(output_strings)
    faults = parse_faults(tabbed_faults)
    coverage = parse_coverage_matrix(tabbed_covs)

    ###########################################################################

    print("Starting simulation for reference circuit...")

    reference_results = simulate(
        connections=connections,
        coverage=coverage,
        outputs=outputs,
        input_values=input_values,
    )

    pprint.pprint(reference_results)

    print("\nStarting simulation for faulty circuit...")

    faulty_results = simulate(
        connections=connections,
        coverage=coverage,
        outputs=outputs,
        input_values=input_values,
        faults=faults
    )

    pprint.pprint(faulty_results)

    ###########################################################################

    print("\nFull difference:")
    diff = get_difference(reference_results, faulty_results)
    pprint.pprint(diff)

    print("\nDifference on outputs:")
    partial_diff = [row[-3:] for row in diff]
    pprint.pprint(
        partial_diff,
        width=30  # such line size forces to print each row from a new line
    )

    ###########################################################################

    tabbed_reachability = """1	1	0	0	0	0	0	0	0	1	0	0
1	1	1	1	0	1	1	1	0	0	1	0
0	0	1	1	1	1	1	0	1	0	0	1"""

    reachability = [
        parse_tabbed_ints(row) for row in tabbed_reachability.split('\n')
    ]

    # diff matrix for testing from the guidelines for the lab
    partial_diff_guide = [
        [1, 0, 0],
        [0, 1, 1],
        [0, 0, 0],
        [0, 0, 0],
        [1, 1, 1]
    ]  # commented

    print("\nBuilding structure activation matrix...")
    activation = get_activation_matrix(partial_diff, reachability)
    pprint.pprint(activation)

    ###########################################################################

    tabbed_fault_matrix = """0	0	0	0	•	0	0	0	0	0	1	1
0	0	•	•	0	0	•	0	0	0	1	1
1	•	1	•	1	•	1	1	1	1	0	0
•	1	•	•	•	1	•	•	•	1	0	0
•	•	•	•	0	0	1	1	0	1	0	1"""

    tabbed_fault_matrix_guide = """0	0	0	0	•	0	0	0	0	0	1	1
1	•	1	•	1	•	1	1	1	1	0	0
•	1	•	1	1	•	1	1	1	1	0	0
•	•	•	•	0	0	1	1	0	1	0	1
0	0	•	•	•	1	•	•	•	0	0	0"""

    print("\nBuilding coded fault matrix...")

    fault_matrix = parse_fault_matrix(tabbed_fault_matrix)
    coded_fault_matrix = get_coded_fault_matrix(fault_matrix)
    pprint.pprint(coded_fault_matrix)

    ###########################################################################

    print("\nPerforming hash superposition...")
    hashed_matrix = hash_superposition(activation, coded_fault_matrix)
    pprint.pprint(hashed_matrix)

    ###########################################################################

    print("\nGetting the list of detected faults...")
    coded_faults = get_coded_fault_list(hashed_matrix)
    detected_faults = decode_fault_list(coded_faults)
    pprint.pprint(detected_faults)

    ###########################################################################

    json_path = "./qc_lab3_results.json"
    full_path = os.path.abspath(json_path)
    print("\nJson output dumped into: {0}".format(full_path))

    with open(json_path, mode='w') as f:
        json.dump({
            'reference': reference_results,
            'faulty': faulty_results,
            'full_diff': diff,
            'outp_diff': partial_diff,
            'activation': activation,
            'coded_fault_t': coded_fault_matrix,
            'detected_faults': detected_faults
        }, fp=f)

    ###########################################################################


if __name__ == "__main__":
    main()
