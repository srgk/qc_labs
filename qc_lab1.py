from collections import OrderedDict


class Element(object):
    def __init__(self):
        self.inputs = list()
        self.operation = None
        self.inverse = False
        self.parent = None

    def add_input(self, element: 'Element'):
        self.inputs.append(element)

    def set_operation(self, op: str):
        self.operation = op

    def apply(self):
        result = []

        if len(self.inputs) == 1:
            if self.inverse:
                for v in self.inputs[0].apply():
                    result.append(not v)

                return result
            else:
                return self.inputs[0].apply()

        assert len(self.inputs) == 2

        el1 = self.inputs[0]
        el2 = self.inputs[1]

        ch1 = el1.apply()
        ch2 = el2.apply()

        for v1 in ch1:
            for v2 in ch2:
                res = None

                if self.operation == '&':
                    res = v1 and v2
                elif self.operation == '|':
                    res = v1 or v2
                elif self.operation == '^':
                    res = (v1 and not v2) or (not v1 and v2)

                assert res is not None

                if self.inverse:
                    res = not res

                result.append(int(res))

        return result

    def __str__(self):
        result = ''

        if self.inverse:
            result += '~'

        result += '('

        for el in self.inputs:
            result += str(el)
            result += self.operation

        if result[-1] != '(':
            result = result[:-1]

        result += ')'

        return result


class Input(object):
    def __init__(self, name: str):
        self.name = name

    def apply(self):
        return [False, True]

    def __str__(self):
        return self.name


def parse(expr: str):
    root = Element()
    inputs = OrderedDict()

    current = root
    inverse_next = False

    for i in range(0, len(expr)):
        if expr[i] == '(':
            el = Element()
            el.inverse = inverse_next
            inverse_next = False
            current.add_input(el)
            el.parent = current
            current = el

        elif expr[i] == ')':
            current = current.parent

        elif expr[i] == '~':
            inverse_next = True

        elif expr[i] == '&' or expr[i] == '|' or expr[i] == '^':
            if current.operation != expr[i]:
                assert current.operation is None
                current.operation = expr[i]

        elif expr[i].isalpha():
            if expr[i] not in inputs:
                inputs[expr[i]] = Input(expr[i])

            current.inputs.append(inputs[expr[i]])

    # print(root)

    res = root.apply()
    print(res)
    print(len(res))


if __name__ == "__main__":
    rawInput = input("Input the expression here: ")
    # rawInput = "(a & b) | ~(c & d)"
    # rawInput = "~((a & b) | ~(c & d))"
    # rawInput = "(a & b) ^ ~(b & d)"

    parse(rawInput)
