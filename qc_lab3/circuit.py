import itertools
from typing import Sequence, List
from .logic import InputElement, LogicElement


class AbsCircuit(object):
    def set_inputs(self, values: Sequence[int]) -> None:
        raise NotImplementedError()

    def get_line_values(self) -> Sequence[bool]:
        raise NotImplementedError()

    def get_output_values(self) -> Sequence[bool]:
        raise NotImplementedError()


class LogicCircuit(AbsCircuit):
    def __init__(self,
                 inputs: Sequence[InputElement],
                 logic: Sequence[LogicElement],
                 output_lines: Sequence[int]
                 ):
        self._inputs = inputs
        self._logic = logic
        self._output_lines = output_lines

    def set_inputs(self, values: Sequence[int]) -> None:
        for i in range(len(values)):
            self._inputs[i].set_value(bool(values[i]))

    def get_line_values(self) -> Sequence[bool]:
        all_elements = itertools.chain(self._inputs, self._logic)
        return [el.evaluate() for el in all_elements]

    def get_output_values(self) -> Sequence[bool]:
        n_inputs = len(self._inputs)
        result = []  # type: List[bool]

        for i in self._output_lines:
            if i < n_inputs:
                el_index = i
                result.append(self._inputs[el_index].evaluate())
            else:
                el_index = i - n_inputs
                result.append(self._logic[el_index].evaluate())

        return result
