from typing import Sequence, List, Iterable, Callable


Predicate = Callable[..., bool]


def bit_vector_to_int(v: Sequence[int]) -> int:
    s = 0

    for i in range(len(v)):
        s += v[i] << i

    return s


def int_to_bit_vector(value: int, size: int) -> Sequence[int]:
    result = []  # type: List[int]

    for i in range(size):
        mask = 1 << i
        bit_value = 1 if value & mask else 0
        result.append(bit_value)

    return result


def check_all(container: Iterable, predicate: Predicate) -> bool:
    for item in container:
        if not predicate(item):
            return False

    return True


def to_int_vector(v: Sequence[str]) -> Sequence[int]:
    return [int(item) for item in v]


def parse_tabbed_ints(row: str) -> Sequence[int]:
    return to_int_vector(row.split('\t'))

