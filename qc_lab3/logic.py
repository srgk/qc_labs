import math
import unittest.mock as mock
from typing import Sequence, List, Collection, Union, TypeVar
from .utils import bit_vector_to_int


ElementCoverage = Sequence[Union[str, int]]


class AbsElement(object):
    @property
    def is_connected(self) -> bool:
        raise NotImplementedError()

    def evaluate(self) -> int:
        raise NotImplementedError()


class InputElement(AbsElement):
    def __init__(self, start_value: bool = False):
        self._value = start_value

    @property
    def is_connected(self) -> bool:
        return True

    def set_value(self, new_value: bool):
        self._value = new_value

    def evaluate(self) -> bool:
        return self._value


def set_input_vector(value: int, inputs: Sequence[InputElement]) -> None:
    for i in range(len(inputs)):
        mask = 1 << i
        bit_value = bool(value & mask)
        inputs[i].set_value(bit_value)


class LogicElement(AbsElement):
    def __init__(self, n_inputs: int, coverage: ElementCoverage):
        self._input_elements = []  # type: List[AbsElement]
        self._n_inputs = n_inputs
        n_vectors = len(coverage)
        n_inputs_covered = math.log(n_vectors, 2)

        if n_inputs_covered != n_inputs:
            raise ValueError("Coverage vector size mismatch")

        self._coverage = coverage

    @property
    def is_connected(self) -> bool:
        return len(self._input_elements) == self._n_inputs

    def set_input_elements(self, els: Collection[AbsElement]) -> None:
        if len(els) != self._n_inputs:
            raise ValueError("Input number mismatch")

        self._input_elements = els

    def evaluate(self) -> bool:
        values = [el.evaluate() for el in self._input_elements]
        index = bit_vector_to_int(values)
        return bool(self._coverage[index])


ElementType = TypeVar('ElementType', bound=AbsElement)


def get_faulty(
        good_element: ElementType, constant_value: bool
) -> ElementType:
    faulty = mock.Mock(spec_set=type(good_element), wraps=good_element)
    faulty.evaluate = lambda: constant_value
    return faulty

