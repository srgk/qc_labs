from typing import Sequence, List, Union, NamedTuple, Optional

from .logic import (
    ElementCoverage,
    InputElement, LogicElement, AbsElement, get_faulty
)
from .circuit import LogicCircuit
from .utils import check_all
from .faults import Fault


ParsedDefinition = NamedTuple(
    'ParsedDefinition', (
        ('elements', List[AbsElement]),
        ('connections', List[Union[Sequence[int], None]])
    )
)


def parse_connections(connections: str) -> Sequence[int]:
    str_cons = connections.split(',')
    return [int(con) - 1 for con in str_cons]


def is_input_element(coverage: ElementCoverage) -> bool:
    return check_all(
        coverage, lambda x: x == '•'
    )


def is_logic_element(coverage: ElementCoverage) -> bool:
    return check_all(
        coverage, lambda x: isinstance(x, int)
    )


def parse_definition(
        connections: Sequence[str],
        coverage: Sequence[ElementCoverage]
) -> ParsedDefinition:
    all_elements = []  # type: List[AbsElement]
    parsed_cons = []  # type: List[Union[Sequence[int], None]]

    n_cons = len(connections)
    n_covs = len(coverage)

    if n_cons != n_covs:
        raise ValueError(
            "Element count mismatch: {0} != {1}".format(n_cons, n_covs)
        )

    for i in range(len(coverage)):
        el_cov = coverage[i]

        if is_input_element(el_cov):
            all_elements.append(InputElement())
            parsed_cons.append(None)

        elif is_logic_element(el_cov):
            el_cons = parse_connections(connections[i])
            el_con_count = len(el_cons)

            parsed_cons.append(el_cons)
            all_elements.append(LogicElement(el_con_count, el_cov))

        else:
            raise ValueError(
                "Invalid coverage vector: {0}".format(str(el_cov))
            )

    return ParsedDefinition(all_elements, parsed_cons)


def connect_element(
        element_id: int,
        all_elements: Sequence[AbsElement],
        connections: Sequence[Optional[Sequence[int]]]
) -> None:
    element = all_elements[element_id]
    el_con_indices = connections[element_id]

    if element.is_connected:
        return

    assert isinstance(element, LogicElement)
    assert el_con_indices is not None  # valid only for input elements

    input_elements = []  # type: List[AbsElement]
    for i in el_con_indices:
        input_elements.append(all_elements[i])
        connect_element(i, all_elements, connections)

    element.set_input_elements(input_elements)


def build_circuit(
        connections: Sequence[str],
        coverage: Sequence[ElementCoverage],
        outputs: Sequence[int],
        faults: Sequence[Fault] = None
) -> LogicCircuit:
    n_cons = len(connections)
    n_covs = len(coverage)

    if n_cons != n_covs:
        raise ValueError(
            "Element count mismatch: {0} != {1}".format(n_cons, n_covs)
        )

    # Parse circuit definition
    all_elements, parsed_cons = parse_definition(connections, coverage)

    # Connect all elements together
    for el_id in range(len(all_elements)):
        if faults is None or faults[el_id] is None:
            connect_element(el_id, all_elements, parsed_cons)
        else:
            all_elements[el_id] = get_faulty(
                all_elements[el_id],
                faults[el_id]
            )

    # Save the circuit
    inputs = []  # type: List[InputElement]
    logic = []  # type: List[LogicElement]

    for el in all_elements:
        if isinstance(el, InputElement):
            inputs.append(el)

        elif isinstance(el, LogicElement):
            logic.append(el)

    return LogicCircuit(inputs, logic, outputs)
